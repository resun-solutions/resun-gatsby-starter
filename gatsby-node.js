const fs = require("fs-extra")
const path = require("path")

exports.onPostBuild = () => {
  console.log("Copying locales")
  fs.copySync(
    path.join(__dirname, "/translations"),
    path.join(__dirname, "/public/translations")
  )
}

// const path = require(`path`)

// exports.createPages = ({ graphql, actions }) => {
//   const { createPage } = actions
//   const productTemplate = path.resolve(`src/templates/produkts.js`)

//   return graphql(`
//     query generateProductPages {
//         allProductsJson {
//             edges {
//               node {
//                 id
//               }
//             }
//         }
//     }
//   `, { limit: 1000 }).then(result => {
//     if (result.errors) {
//       throw result.errors
//     }

//     result.data.allProductsJson.edges.forEach((edge, id) => {    
//       console.log(edge.node.id);  
//         createPage({
//         path: "/produkts/"+id,
//         component: productTemplate,
//         context: {
//             pageid: edge.node.id
//         },
//         })
//     })
//   })
// }